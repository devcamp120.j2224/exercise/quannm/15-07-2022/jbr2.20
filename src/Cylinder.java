public class Cylinder extends Circle {
    double height = 1.0;

    public Cylinder() {
        return;
    }
    public Cylinder(double radius) {
        Circle circle = new Circle();
        this.radius = radius;
    }
    public Cylinder(double radius, double height) {
        Circle circle = new Circle();
        this.radius = radius;
        this.height = height;
    }
    public Cylinder(double radius, double height, String color) {
        Circle circle = new Circle();
        this.radius = radius;
        this.height = height;
        this.color = color;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    public double getVolume() {
        return getArea() * getHeight();
    }
    @Override
    public String toString() {
        return "Cylinder[radius=" + getRadius() + ",height=" + getHeight() + ",color=" + getColor() + "]";
    }
}
