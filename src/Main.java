public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, "green");
        System.out.println(circle1 + "," + circle2 + "," + circle3);
        System.out.println(circle1.getArea() + "," + circle2.getArea() + "," + circle3.getArea());

        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5, 1.5);
        Cylinder cylinder4 = new Cylinder(3.5, 1.5, "blue");
        System.out.println("Details cylinder1=" + cylinder1);
        System.out.println("Volume cylinder1=" + cylinder1.getVolume());
        System.out.println("Details cylinder2=" + cylinder2);
        System.out.println("Volume cylinder2=" + cylinder2.getVolume());
        System.out.println("Details cylinder3=" + cylinder3);
        System.out.println("Volume cylinder3=" + cylinder3.getVolume());
        System.out.println("Details cylinder4=" + cylinder4);
        System.out.println("Volume cylinder4=" + cylinder4.getVolume());
    }
}